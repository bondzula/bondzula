import Vue from 'vue';
var VueScrollTo = require('vue-scrollto');

Vue.use(VueScrollTo, {
  container: 'body',
  duration: 500,
  easing: 'ease',
  cancelable: true,
  x: false,
  y: true
});
